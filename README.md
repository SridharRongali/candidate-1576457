**Technical Assessment - Candidate 1576457**
----------------------------------------
Steps to execute automation script:
----------------------------------------
1) Open the project in IDE.
2) Make sure all the dependencies in pom.xml are downloaded.
3) Execute the script using the Runner file in IDE or using 'mvn clean verify' in the command line tool from the project root folder.
4) Check the script execution reports/results in below folders (created after the script execution):
    - Report location: target --> failsafe-reports --> failsafe-summary.xml
    - Report location: target --> site --> serenity --> scripts --> index.html
	
	
