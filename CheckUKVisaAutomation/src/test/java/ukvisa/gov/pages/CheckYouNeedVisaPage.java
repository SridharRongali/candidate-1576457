package ukvisa.gov.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class CheckYouNeedVisaPage extends PageObject {
	
	@FindBy(xpath = ".//input[@value='tourism']")
	private WebElementFacade radioTourism;
	//WebElement radioTourism; // = getDriver().findElement(By.id(".//input[@value='tourism']"));

	@FindBy(xpath = ".//input[@value='study']")
	private WebElementFacade  radioStudy;
	//WebElement radioStudy; //  = getDriver().findElement(By.xpath(".//input[@value='study']"));
	
	@FindBy(xpath = ".//input[@value='six_months_or_less']")
	private WebElementFacade radioSixMonthsOrLess;
	//WebElement radioSixMonthsOrLess; // = getDriver().findElement(By.xpath(".//input[@value='six_months_or_less']"));

	@FindBy(xpath = ".//input[@value='longer_than_six_months']")
	private WebElementFacade radioLongerThanSixMonths;
	//WebElement radioLongerThanSixMonths; //  = getDriver().findElement(By.xpath(".//input[@value='longer_than_six_months']"));

	@FindBy(xpath = "(.//button[@type='submit'])[2]")
	private WebElementFacade btnNextStep;
	//WebElement btnNextStep; //  = getDriver().findElement(By.xpath("(.//button[@type='submit'])[2]"));
	
	
	@FindBy(xpath = ".//div[@id='result-info']//div/h2")
	private WebElementFacade labelResult;
	//WebElement labelResult; //  = getDriver().findElement(By.xpath(".//div[@id='result-info']//div/h2"));
	
	

	/**
	 * Select reason for Visa
	 * 
	 * @param reason
	 */
	public void selectReasonForVisa(String reason) throws InterruptedException{
		Thread.sleep(5000);
		//WebElement radioStudy = getDriver().findElement(By.xpath(".//input[@value='study']"));
		//WebElement radioTourism = getDriver().findElement(By.xpath(".//input[@value='tourism']"));
		
		switch (reason.toUpperCase()) {
		case "TOURISM":
			element(radioTourism).waitUntilVisible().click();
			break;
		case "STUDY":
			element(radioStudy).waitUntilVisible().click();
			break;
		}
	}

	/**
	 * Click on next step
	 */
	public void clickOnNextStep() {
		element(btnNextStep).waitUntilVisible().click();
	}

	/**
	 * Selects duration of study
	 * 
	 * @param studyDuration
	 */
	public void selectStudyDuration(String studyDuration) {
		if (studyDuration.toLowerCase().equals("more than 6 months"))

			element(radioLongerThanSixMonths).waitUntilVisible().click();
		else
			element(radioSixMonthsOrLess).waitUntilVisible().click();

	}

	/**
	 * Gives the visa result text
	 * 
	 * @return
	 */
	public String getResultText() {
		return element(labelResult).waitUntilVisible().getText();
	}
}