package ukvisa.gov.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.gov.uk/check-uk-visa/y")
public class CheckUKVisaHomePage extends PageObject {
	@FindBy(xpath = ".//p[@id='get-started']/a")
	private static WebElementFacade btnStartNow;
	@FindBy(id = "response")
	private static WebElementFacade dropDownNationality;
	@FindBy(xpath = "(.//button[@type='submit'])[2]")
	private static WebElementFacade btnNextStep;
	@FindBy(xpath = "(.//button[@data-track-category='cookieBanner'])[1]")
	private static WebElementFacade btnAcceptCookies;

	/**
	 * Select Nationality
	 * 
	 * @param nationality
	 * @throws InterruptedException
	 */
	public void selectNationality(String nationality){
		dropDownNationality.selectByVisibleText(nationality);
	}

	public void GoToCheckYouNeedAVisa() {
		btnAcceptCookies.waitUntilClickable().click();
	}
	
	/**
	 * Click on next step
	 */
	public void clickOnNextStep() {
		btnNextStep.waitUntilVisible().click();
	}
}
