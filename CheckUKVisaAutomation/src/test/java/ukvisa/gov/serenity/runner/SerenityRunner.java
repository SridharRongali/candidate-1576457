package ukvisa.gov.serenity.runner;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/features/CheckUKVisa.feature",glue="ukvisa.gov.stepDefs")
public class SerenityRunner {

}
