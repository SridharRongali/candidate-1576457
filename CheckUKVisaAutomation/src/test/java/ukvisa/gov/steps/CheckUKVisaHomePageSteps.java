package ukvisa.gov.steps;

import net.thucydides.core.annotations.Step;
import ukvisa.gov.pages.CheckUKVisaHomePage;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class CheckUKVisaHomePageSteps {
	CheckUKVisaHomePage homePage;

	@Step
	public void selectNationality(String nationality) {
		ChromeOptions chromeOptions = new ChromeOptions();
		ChromeDriver chromeDriver = new ChromeDriver(chromeOptions);
		chromeDriver.manage().window().maximize();
		chromeDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		homePage.setDriver(chromeDriver);
		homePage.getDriver().manage().deleteAllCookies();
		homePage.open();
		homePage.GoToCheckYouNeedAVisa();
		homePage.selectNationality(nationality);
		homePage.clickOnNextStep();
	}


}
