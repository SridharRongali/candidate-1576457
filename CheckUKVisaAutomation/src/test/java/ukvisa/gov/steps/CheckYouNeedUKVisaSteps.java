package ukvisa.gov.steps;

import org.junit.Assert;
import net.thucydides.core.annotations.Step;
import ukvisa.gov.pages.CheckYouNeedVisaPage;


public class CheckYouNeedUKVisaSteps {

	CheckYouNeedVisaPage checkYouNeedVisaPage;

	@Step
	public void selectTheReasonForVisa(String reason) throws InterruptedException {
		Thread.sleep(5000);
		
		/**ChromeOptions chromeOptions = new ChromeOptions();
		ChromeDriver chromeDriver = new ChromeDriver(chromeOptions);
		String currentURL = chromeDriver.getCurrentUrl();
		chromeDriver.close();
		chromeDriver.get(currentURL);
		checkYouNeedVisaPage.getDriver().switchTo().activeElement();
		checkYouNeedVisaPage.clickOnNextStep();
		**/
		checkYouNeedVisaPage.selectReasonForVisa(reason);

	}

	@Step
	public void submitTheForm() {
		checkYouNeedVisaPage.clickOnNextStep();
	}

	@Step
	public void selctTheDurationForStudy(String duration) {
		checkYouNeedVisaPage.selectStudyDuration(duration);
		checkYouNeedVisaPage.clickOnNextStep();
	}

	@Step
	public void thenIshouldNeedAVisa(String resultText) {
		Assert.assertEquals(resultText, checkYouNeedVisaPage.getResultText());
	}

}
