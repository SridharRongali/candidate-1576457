package ukvisa.gov.stepDefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import ukvisa.gov.steps.CheckYouNeedUKVisaSteps;

public class CheckYouNeedVisaStepDefs {

	@Steps
	private CheckYouNeedUKVisaSteps checkVisaSteps;

	@Given("^I select the reason \"(.*?)\"$")
	public void i_select_the_reason(String reasonForVisa) throws Throwable {
		checkVisaSteps.selectTheReasonForVisa(reasonForVisa);
	}

	@Given("^I state I am intending to stay for \"(.*?)\"$")
	public void i_state_I_am_intending_to_stay_for(String duration) throws Throwable {
		checkVisaSteps.selctTheDurationForStudy(duration);

	}

	@When("^I submit the form$")
	public void i_submit_the_form() throws Throwable {
		checkVisaSteps.submitTheForm();
	}

	@Then("^I will be informed \"(.*?)\"$")
	public void i_will_be_informed(String needVisaOrnot) throws Throwable {
		checkVisaSteps.thenIshouldNeedAVisa(needVisaOrnot);
	}

	@Given("^I state I am not travelling with or visiting a partner or family$")
	public void i_state_I_am_not_travelling_with_or_visiting_a_partner_or_family() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

	}

}
