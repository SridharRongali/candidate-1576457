package ukvisa.gov.stepDefs;

import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;
import ukvisa.gov.steps.CheckUKVisaHomePageSteps;

public class CheckUKVisaHomePageStepDefs {
	@Steps
	CheckUKVisaHomePageSteps homePageSteps;

	@Given("^I provide a nationality of \"(.*?)\"$")
	public void i_provide_a_nationality_of(String nationality) throws Throwable {
		homePageSteps.selectNationality(nationality);

	}

}
