package com.postcodesapi.runner;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/features/Api.feature",glue="com.api.sepDef")
public class PostcodesApiRunner {

}
