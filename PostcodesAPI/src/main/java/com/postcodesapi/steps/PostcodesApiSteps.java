package com.postcodesapi.steps;

import com.postcodesapi.requests.PostcodesApiRequests;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;

public class PostcodesApiSteps {

	@Step
	public Response SendApiRequest() throws InterruptedException {
		return PostcodesApiRequests.getPostcodesReponse();

	}
}
