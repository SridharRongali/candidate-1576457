package com.postcodesapi.stepDefs;

import org.junit.Assert;

import com.postcodesapi.steps.PostcodesApiSteps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;

public class PostcodesApiStepDefs {

	@Steps
	private PostcodesApiSteps postcodeApiSteps;

	Response JsonResponse=null;

	@When("^I send a get request to api postcodes$")
	public void i_send_a_get_request_to_api_postcodes() throws Throwable {
		JsonResponse = postcodeApiSteps.SendApiRequest();
	}

	@Then("^I get a \"([^\"]*)\" response$")
	public void i_get_a_response(String postCode) throws Throwable {
		Assert.assertEquals(postCode, JsonResponse.jsonPath().getString("status"));
	}

}
