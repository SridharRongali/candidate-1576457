package com.postcodesapi.requests;

import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

import com.postcodesapi.utilities.GetConfigValues;

public class PostcodesApiRequests {

	/**
	 * Get post codes
	 * @return
	 */
	public static Response getPostcodesReponse() {
		Response jsonRespone = given().contentType("application/json").when().get(GetConfigValues.baseUrl);
		return jsonRespone;
	}

}
