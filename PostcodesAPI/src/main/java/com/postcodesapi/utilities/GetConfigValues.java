package com.postcodesapi.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class GetConfigValues {

	public static String baseUrl = getProperties("config.properties", "base_url");

	public static String getProperties(String fileName, String key) {
		FileInputStream iFile = null;
		Properties allProps = null;
		try {
			iFile = new FileInputStream(System.getProperty("user.dir") + "\\config\\" + fileName);
			allProps = new Properties();
			allProps.load(iFile);
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			try {
				iFile.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return allProps.getProperty(key);
	}
}
